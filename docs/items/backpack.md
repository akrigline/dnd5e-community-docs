# Backpack

![Up to date as of 1.4.2](https://img.shields.io/badge/dnd5e-v1.4.2-informational)

## Configuration

The tabs that follow explain what each field is meant to control about a given item.

<!-- tabs:start -->

### **Description**

[_description-physical](_description-physical.md ':include')

### **Details**

![Details panel of a Backpack Item](./_images/backpack/backpack-details.png)

#### Container Details

##### Capacity

##### Capacity Type

##### Container Properties

###### Weightless Contents

<!-- tabs:end -->

## Usage

Backpack items do not have a workflow attached, they simply print their description to chat.

## Automations

None
