# Feat

![Up to date as of 1.4.2](https://img.shields.io/badge/dnd5e-v1.4.2-informational)

## Configuration

<!-- tabs:start -->
### **Description**

### **Details**

#### Feature Usage

[_activation](_activation.md ':include')

##### Action Recharge

#### Feature Attack

[_action](_action.md ':include')

### **Effects**

See: [Active Effects](active-effects.md)

<!-- tabs:end -->
## Usage

## Automations
