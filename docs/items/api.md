# Item Macro API

![Up to date as of 1.4.2](https://img.shields.io/badge/dnd5e-v1.4.2-informational)

This page contains information about how to interact with `Item5e` through the Javascript API that might be useful for [Macros](https://foundryvtt.com/article/macros/) related to [Items](items/items.md).

## `Item5e#roll(configureDialog, rollMode, createMessage)`

Roll the item to Chat, creating a chat card which contains follow up attack or damage roll options

### Parameters

| Name      | Type             | Description                                    |
|-----------|------------------|------------------------------------------------|
| `options`    | `object`   | Options which configure the item roll         |

### Example Snippets

```javascript
item.roll();
```
