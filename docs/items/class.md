# Class

![Up to date as of 1.4.2](https://img.shields.io/badge/dnd5e-v1.4.2-informational)

## Configuration
<!-- tabs:start -->

### **Description**

### **Details**

<!-- tabs:end -->
## Usage

Class items do not have a workflow attached, they simply print their description to chat.

## Automations

- Level Up Automation
- Subclasses
- Hit Dice
