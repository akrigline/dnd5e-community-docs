# Weapons

![Up to date as of 1.4.2](https://img.shields.io/badge/dnd5e-v1.4.2-informational)

## Configuration

<!-- tabs:start -->

### **Description**

[_description-physical](_description-physical.md ':include')

### **Details**

#### Weapon Details

#### Weapon Usage

[_action](_action.md ':include')

#### Weapon Attack

[_activation](_activation.md ':include')

- Configuring an Attack
- Setting up damage formula; what inline attributes are available?
- Magical Bonuses
- Weapon Properties; which actually do things? Versatile, Finesse, Heavy?

### **Effects**

See: [Active Effects](active-effects.md)

<!-- tabs:end -->

## Usage

- Usual Workflow for Attack/Save + Damage Chat Cards

## Automations
