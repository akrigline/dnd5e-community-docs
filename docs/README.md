# Home

The goal of this community-powered documentation effort is to be the definitive guide to playing 5e with the vanilla system in written form so as to be searchable and discoverable.

## [System Settings](system-settings.md)

A brief rundown of what each setting in the system settings does.

## [Actors](actors/actors.md)

How to configure and use each type of Actor.

- [Player Characters](actors/player-characters.md)
- [NPCs](actors/npcs.md)
- [Vehicles](actors/vehicles.md)

### [Actor Macro API](actors/api.md)

Contains documentation and examples for the javascript api to interact with Actors.

## [Items](items/items.md)

How to configure and use each type of Item.

- [Weapon](items/weapon.md)
- [Equipment](items/equipment.md)
- [Consumable](items/consumable.md)
- [Tool](items/tool.md)
- [Loot](items/loot.md)
- [Class](items/class.md)
- [Spell](items/spell.md)
- [Feature](items/feature.md)
- [Backpack](items/backpack.md)

### [Item Macro API](items/api.md)

Contains documentation and examples for the javascript api to interact with Items.

## [Included Compendiums](included-compendiums.md)

Details the contents of each of the included compendiums:

- [Monsters (SRD)](included-compendiums.md?id=monsters-srd)
- [Starter Heroes](included-compendiums.md?id=starter-heroes)
- [Class Features (SRD)](included-compendiums.md?id=class-features-srd)
- [Classes (SRD)](included-compendiums.md?id=classes-srd)
- [Items (SRD)](included-compendiums.md?id=items-srd)
- [Monster Features (SRD)](included-compendiums.md?id=monster-features-srd)
- [Racial Features (SRD)](included-compendiums.md?id=racial-features-srd)
- [Rules (SRD)](included-compendiums.md?id=rules-srd)

## [Active Effects](active-effects.md)

How to configure and use Active Effects.

## Guides

- [Polymorph](guides/polymorph.md)
- [Running Combat](guides/running-combat.md)
- [Troubleshooting Roll Bonuses](guides/troubleshooting-roll-bonuses.md)

---

Except as otherwise noted, the content of these documents is licensed under the [Creative Commons Attribution-ShareAlike 4.0 License](https://creativecommons.org/licenses/by-sa/4.0/), and code samples are licensed under the Unlicense (text below).

<details>
<summary>The Unlicense</summary>

This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or distribute this software, either in source code form or as a compiled binary, for any purpose, commercial or non-commercial, and by any means.

In jurisdictions that recognize copyright laws, the author or authors of this software dedicate any and all copyright interest in the software to the public domain. We make this dedication for the benefit of the public at large and to the detriment of our heirs and successors. We intend this dedication to be an overt act of relinquishment in perpetuity of all present and future rights to this software under copyright law.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to [http://unlicense.org/](http://unlicense.org/)

</details>
