# Included Compendiums

![Up to date as of 1.4.2](https://img.shields.io/badge/dnd5e-v1.4.2-informational)

!> Removing or editing entries inside these compendiums can cause unexpected behavior when interacting with the 5e system. Additionally, anything edited will be reset to default when the system is updated.

?> If you wish to edit something for your table, the best thing to do is to import that thing into the world and make changes to that World-level document.

## Actor

### Monsters (SRD)

All of the [NPCs](actors/npcs.md) in the 5e SRD pre-configured for Foundry use. These monsters are the best place to look as a starting point to create your own monsters.

### Starter Heroes

Some pre-made level 1 [Player Characters](actors/player-characters.md using only SRD material. These are the best way to see examples of characters already-configured for each SRD class.

## Item

### Class Features (SRD)

Contains all of the SRD active and passive Class [Feautures](items/feature.md) as items. This compendium is used to draw class features from during the Character Level Up automations.

### Classes (SRD)

Contains the SRD [Class](items/class.md) items. These are the items which drive the built-in Character Level Up automations. These are also the best examples to look at for how to set up homebrew classes.

### Items (SRD)

Contains mundane and magical [Weapons](items/weapon.md), [Equipment](items/equipment.md), [Consumables](items/consumable.md), [Backpack](items/backpack.md), [Tool](items/tool.md), and [Loot](items/loot.md) items from the SRD.

Also includes some class items (e.g. Book of Shadows from Warlock Pact Boons)

### Monster Features (SRD)

Contains passive and active [Feature](items/feature.md) items for every monster ability in the SRD. Use these to create your own monsters with the same abilities as other monsters by simply dragging and dropping from the compendium onto your NPC actor.

### Racial Features (SRD)

Contains SRD Race items as well as the passive and active [Features](items/feature.md) those races provide.

### Spells (SRD)

Contains all of the SRD [Spell](items/spell.md) items with attacks and saves pre-configured.

### Trade Goods (SRD)

[Loot](items/loot.md) items from the SRD table about Trade Goods.

## JournalEntry

### Rules (SRD)

An interlinked compendium containing all of the Rules in the SRD. Contains some content from the Freely Available Basic Rules.
