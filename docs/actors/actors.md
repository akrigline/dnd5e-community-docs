# Actors

![Up to date as of 1.4.2](https://img.shields.io/badge/dnd5e-v1.4.2-informational)

## Common Configuration

- HP
- Armor Class
- Attributes
- Skills
- Size
- Damage Resistance/Immunity/Vulneratiblity
- Condition Immunity

## Types

What does each Item type do differently from the rest?

|    Type    | Description |
|:----------:|-------------|
|   [Character](items/player-characters.md)   | PCs.             |
|  [NPC](items/npcs.md) | NPCs, Monsters, Familiars, etc.             |
| [Vehicle](items/vehicles.md) | Boats, Wagons, Infernal Machines            |

## Usage

- Spawning tokens on a map
- Changing HP via HUD?

### Macro and API usage

_Main Article: [Actor API](actors/api.md)_

## Automations

- Token Size based on Creature Size
- Armor Class Calculation & Effects

## Token Configuration

- Main Article: [Foundry KB](https://foundryvtt.com/article/tokens/)
- Resources: TempHP Bar
- Vision: Darkvision Settings
