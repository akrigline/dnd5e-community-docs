# NPCs

![Up to date as of 1.4.2](https://img.shields.io/badge/dnd5e-v1.4.2-informational)

## Configuration

- CR and Proficiency Bonus
- Adding/Configuring Legendary Actions
- Adding Lair Actions
- Token Configuration

## Usage

- Unlinked Tokens and Wildcard Images

## Automations
