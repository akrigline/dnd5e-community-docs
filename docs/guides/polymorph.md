# Polymorph

![Up to date as of 1.4.2](https://img.shields.io/badge/dnd5e-v1.4.2-informational)

## Unsoluble's Guide: [Using 5e Transformations in Foundry](<https://www.youtube.com/watch?v=YjcugG_akk4>)

<iframe width="560" height="315" src="https://www.youtube.com/embed/YjcugG_akk4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- How do you do it?
- What do the settings mean?
