# Running Combat

![Up to date as of 1.4.2](https://img.shields.io/badge/dnd5e-v1.4.2-informational)

1. Entering Combat State and Rolling Initiative
2. Rolling Attacks, Fastforwarding the Dialog (keyboard shortcuts)
3. Rolling Damage, Fastforwarding the Dialog (keyboard shortcuts)
4. Rolling Spells, Placing Templates
5. Applying Damage (always to the selected token, not the targeted one)
6. Rolling Saving Throws (always selected token, not targeted token)
7. Applying Effects to actors
